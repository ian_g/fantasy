# fantasy sports

because yahoo fantasy won't let us have different scoring systems each week
built with flask and sqlalchemy  

## running

development started on python 3.10
it might work on python 3.6 (it uses f-strings), but idk  
from the repository root directory

    python -mvenv .
    source bin/activate
    python -mpip install -r requirements.txt
    git config --local core.hooksPath .hooks
    flask db upgrade
    flask run #It's a dev server, not for production

the site will be empty - there's no data  

if you'd like to fix your own data up, there's a database diagram in misc/ that loads very nicely in Ondřej Žára's [sql designer](https://ondras.zarovi.cz/sql/demo/)

## testing

there should be tests. eventually.  
the quality is debatable, but they've made some updates much simpler

    coverage run [&& coverage report && coverage html]

## todo

1. APScheduler without flask-apscheduler
1. Create person import
1. Create game import to game, game\_event, event\_person
1. League creation page
    - Must be logged in
    - Requires name
    - Set position breakdown
    - Set veto count
    - Set veto time
    - Set weekly acquisition limit
    - Set playoff team
    - Set draft time
    - Set self as commissioner
    - Set initial league scoring
    - Set your team's name
    - Creates league record
    - Creates league\_commissioner's team
    - Creates league\_player record for each active person. Leave team empty
1. Team creation page
    - Runs once for your team when league created
    - Team name
    - Shareable URL for others to join
        - Redirect to login
        - Option for team name
1. Draft
    - 30 minutes pre-draft create picks. Random order then snake
    - When pick player, create team\_roster records
1. Roster moves
    - Dropping player: remove unlocked team\_roster records
    - Adding player: create team\_roster records
    - Trades?
    - Waiver claims?
1. Matchups
    - Freezing rosters
    - Calculating scores
    - Updating league standings
    - Start active players button
1. Admin settings?
