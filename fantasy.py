import click
from app import create_app, db
from app.models import User

app = create_app("default")


@app.shell_context_processor
def make_shell_context():
    return {
        "db": db,
    }


"""
@app.cli.command()
@click.option(
    "--coverage/--no-coverage",
    "check_coverage",
    default=False,
    help="Run tests under coverage.py",
)
@click.argument("names", nargs=-1)
def test(check_coverage, names):
    import unittest

    if check_coverage:
        import coverage

        cov = coverage.coverage(branch=True, include="app/*")
        cov.start()
    if names:
        tests = unittest.TestLoader().loadTestsFromNames(names)
    else:
        tests = unittest.TestLoader().discover("test")
    unittest.TextTestRunner(verbosity=2).run(tests)
    if check_coverage:
        cov.stop()
        cov.save()
        print("Summary: ")
        cov.report()
        cov.erase()
"""
