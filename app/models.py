from datetime import date, datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db, login


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, index=True, unique=True)
    email = db.Column(db.String, index=True, unique=True)
    password_hash = db.Column(db.String)
    created = db.Column(db.Date, default=date.today())
    bio = db.Column(db.String)
    last_active = db.Column(db.DateTime, default=datetime.utcnow)
    admin = db.Column(db.Boolean, default=False)

    teams = db.relationship("LeagueTeam")

    def __repr__(self):
        return f"<User {self.username}>"

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Configuration(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String, unique=True)
    value = db.Column(db.String)


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nhl_id = db.Column(db.Integer, unique=True)
    ep_id = db.Column(db.Integer, unique=True)
    name = db.Column(db.String, index=True)
    birthdate = db.Column(db.Date)
    nationality = db.Column(db.String)
    position = db.Column(db.String)
    height = db.Column(db.Integer)
    weight = db.Column(db.Integer)
    number = db.Column(db.Integer)
    shoots = db.Column(db.String)
    active = db.Column(db.Boolean, default=False)
    rookie = db.Column(db.Boolean, default=False)
    captain = db.Column(db.Boolean, default=False)
    alternate_captain = db.Column(db.Boolean, default=False)
    non_roster = db.Column(db.Boolean, default=True)

    def __repr__(self):
        return f"{self.name} ({self.id})"


class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)
    abbreviation = db.Column(db.String, unique=True)
    team_name = db.Column(db.String)
    team_location = db.Column(db.String)
    logo = db.Column(db.String)

    def __repr__(self):
        return f"{self.name} ({self.abbreviation})"


class Season(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    start = db.Column(db.Date)
    end = db.Column(db.Date)

    def __repr__(self):
        return f"{self.name}"


class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    start_time = db.Column(db.DateTime)
    home_team_id = db.Column(db.Integer, db.ForeignKey("team.id"))
    away_team_id = db.Column(db.Integer, db.ForeignKey("team.id"))

    home_team = db.relationship("Team", foreign_keys=[home_team_id])
    away_team = db.relationship("Team", foreign_keys=[away_team_id])

    def __repr__(self):
        return f"{self.id} - {self.start_time}"


class GameEvent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    game_id = db.Column(db.Integer, db.ForeignKey("game.id"))
    event_id = db.Column(db.Integer)
    period = db.Column(db.Integer)
    goals_home = db.Column(db.Integer)
    goals_away = db.Column(db.Integer)
    team_id = db.Column(db.Integer, db.ForeignKey("team.id"))
    x_coord = db.Column(db.Integer)
    y_coord = db.Column(db.Integer)
    period_type = db.Column(db.Time)
    period_time = db.Column(db.Time)
    period_time_remaining = db.Column(db.Time)
    timestamp = db.Column(db.DateTime)
    event_code = db.Column(db.String)
    type = db.Column(db.String)
    description = db.Column(db.String)

    team = db.relationship("Team")
    game = db.relationship("Game")

    def __repr__(self):
        return f"{self.period_time}: {self.type}"


class EventPerson(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer, db.ForeignKey("game_event.id"))
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    type = db.Column(db.String)

    event = db.relationship("GameEvent")
    person = db.relationship("Person")


class League(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    name = db.Column(db.String)
    draft_time = db.Column(db.DateTime)
    trade_deadline = db.Column(db.DateTime)
    trade_veto_count = db.Column(db.Integer)
    trade_veto_time = db.Column(db.Time)
    weekly_acquisition_limit = db.Column(db.Integer)

    season = db.relationship("Season")

    def __repr__(self):
        return f"{self.id} - {self.name}"


class LeaguePosition(db.Model):
    """For player positions. If you want C, RW, and LW to all be F, set position to F and aliases to C,RW,LW"""

    id = db.Column(db.Integer, primary_key=True)
    league_id = db.Column(db.Integer, db.ForeignKey("league.id"))
    position = db.Column(db.Integer)
    aliases = db.Column(db.String)
    count = db.Column(db.Integer)

    league = db.relationship("League")

    def __repr__(self):
        return f"Position: {self.position} ({self.aliases})"


class LeagueScoring(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    league_id = db.Column(db.Integer, db.ForeignKey("league.id"))
    valid_from = db.Column(db.Date)
    valid_until = db.Column(db.Date)
    statistic_id = db.Column(db.Integer, db.ForeignKey("scoring_statistic.id"))
    value = db.Column(db.Integer)

    league = db.relationship("League")
    statistic = db.relationship("ScoringStatistic")

    def __repr__(self):
        return f"{self.statistic.name}: {self.value} from {self.valid_from} -> {self.valid_to}"


class ScoringStatistic(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    def __repr__(self):
        return f"{self.name}"


class LeagueTeam(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    league_id = db.Column(db.Integer, db.ForeignKey("league.id"))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    commissioner = db.Column(db.Boolean, default=False)
    waiver_priority = db.Column(db.Integer)
    wins = db.Column(db.Integer, default=0)
    losses = db.Column(db.Integer, default=0)
    ties = db.Column(db.Integer, default=0)

    league = db.relationship("League")
    user = db.relationship("User", back_populates="teams")

    def __repr__(self):
        return f"{self.name} - {self.league.name}"


class LeaguePerson(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    league_id = db.Column(db.Integer, db.ForeignKey("league.id"))
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    team_id = db.Column(db.Integer, db.ForeignKey("league_team.id"))

    league = db.relationship("League")
    team = db.relationship("LeagueTeam")
    person = db.relationship("Person")


class TeamRoster(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, default=date.today())
    locks_at = db.Column(db.DateTime)
    team_id = db.Column(db.Integer, db.ForeignKey("league_team.id"))
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    position = db.Column(db.String)

    team = db.relationship("LeagueTeam")
    person = db.relationship("Person")

    def __repr__(self):
        return f"Roster {self.date} - {self.person.name} ({self.position})"


class DraftPick(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    league_id = db.Column(db.Integer, db.ForeignKey("league.id"))
    team_id = db.Column(db.Integer, db.ForeignKey("league_team.id"))
    selection_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    round = db.Column(db.Integer)
    position = db.Column(db.Integer)

    league = db.relationship("League")
    team = db.relationship("LeagueTeam")
    selection = db.relationship("Person")

    def __repr__(self):
        return f"{self.selection.name} -> {self.team.name}"


class LeagueMatchup(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    league_id = db.Column(db.Integer, db.ForeignKey("league.id"))
    home_team_id = db.Column(db.Integer, db.ForeignKey("league_team.id"))
    away_team_id = db.Column(db.Integer, db.ForeignKey("league_team.id"))
    home_points = db.Column(db.Integer)
    away_points = db.Column(db.Integer)
    week_starts_on = db.Column(db.Date)

    home_team = db.relationship("LeagueTeam", foreign_keys=[home_team_id])
    away_team = db.relationship("LeagueTeam", foreign_keys=[away_team_id])

    def __repr__(self):
        return f"{self.away_team.name} @ {self.home_team.name}"
