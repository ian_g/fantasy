import json
from flask import (
    render_template,
    url_for,
    flash,
    session,
    abort,
    jsonify,
    current_app,
    g,
)
from flask_login import current_user
from .. import db
from . import bp

# from .forms import SearchForm
# @bp.before_app_request
# def search_form():
#    g.search_form = SearchForm()


@bp.route("/index")
@bp.route("/")
def index():
    return render_template("index.html", title="Home", css="homepage.css")


@bp.route("/about/")
@bp.route("/about")
def about():
    return render_template("about.html", title="About", css="about.css")
