import json
from flask import redirect, url_for, abort, request
from flask_login import current_user
from flask_admin import Admin, AdminIndexView
from flask_admin.model import typefmt
from flask_admin.contrib.sqla import ModelView
from markupsafe import Markup

from .. import db, admin
from ..models import (
    User,
    Configuration,
    Person,
)


def bool_formatter(view, value):
    if value:
        return "True"
    return "False"


def currency_formatter(value):
    if not value:
        return ""
    return "${:,}".format(value)


FORMATTERS = {
    type(None): typefmt.null_formatter,
    bool: bool_formatter,
    list: typefmt.list_formatter,
    dict: typefmt.dict_formatter,
}


class ModelViewV2(ModelView):
    can_view_details = True
    column_display_pk = True
    column_hide_backrefs = False
    column_type_formatters = FORMATTERS
    can_set_page_size = True
    action_disallowed_list = ["delete"]
    extra_css = [
        "/static/css/base.css",
        "/static/css/flask-admin.css",
    ]

    def is_accessible(self):
        return current_user.is_authenticated and current_user.admin == True

    def inaccessible_callback(self, name, **kwargs):
        if not current_user.is_authenticated:
            return redirect(url_for("user.login", next=request.url))
        abort(403)

    # edit_template = None   #Custom template to ignore this arg entirely
    # list_template = #Custom template
    # create_template = #Decide if need this. If so, custom template. If not, None


class PersonView(ModelViewV2):
    column_list = [
        "id",
        "nhl_id",
        "ep_id",
        "name",
        "birthdate",
        "nationality",
        "position",
        "height",
        "weight",
        "shoots",
        "active",
        "number",
    ]
    column_details_list = [
        "id",
        "nhl_id",
        "ep_id",
        "name",
        "birthdate",
        "nationality",
        "position",
        "height",
        "weight",
        "shoots",
        "active",
        "rookie",
        "captain",
        "alternate_captain",
        "non_roster",
        "number",
    ]
    column_editable_list = [
        "position",
        "height",
        "shoots",
        "weight",
        "active",
        "number",
    ]
    column_searchable_list = [
        "name",
        "height",
        "weight",
        "position",
        "active",
        "number",
    ]
    column_sortable_list = [
        "name",
        "height",
        "position",
        "active",
        "number",
    ]
    column_labels = {
        "nhl_id": "NHL Player ID",
        "ep_id": "EliteProspects Player ID",
    }


class ConfigurationView(ModelViewV2):
    column_list = ["id", "key", "value"]
    column_details_list = column_list
    column_editable_list = ["value"]
    column_searchable_list = ["key"]
    column_sortable_list = ["key", "value"]
    column_labels = {
        "key": "Setting Name",
        "value": "Setting Value",
    }
    form_widget_args = {
        "key": {
            "disabled": True,
        }
    }


admin.add_views(
    PersonView(Person, db.session, category="Player Data"),
    ConfigurationView(Configuration, db.session, category="Site Data"),
)
